﻿using Lab3.Contract;
using Lab3.Implementation;
using System;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IGlosnik);
        public static Type I2 = typeof(IWyswietlacz);
        public static Type I3 = typeof(IZasilacz);

        public static Type Component = typeof(TelefonSatelitarny);

        //public static GetInstance GetInstanceOfI1 = (component) => { return new Glosnik(); };
        //public static GetInstance GetInstanceOfI2 = (component) => { return new Wyswietlacz(); };
        //public static GetInstance GetInstanceOfI3 = (component) => { return new Zasilanie(); };

        public static GetInstance GetInstanceOfI1 = (Component) => Component;
        public static GetInstance GetInstanceOfI2 = (Component) => Component;
        public static GetInstance GetInstanceOfI3 = (Component) => Component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Mixin);
        public static Type MixinFor = typeof(IZasilacz);

        #endregion
    }
}
