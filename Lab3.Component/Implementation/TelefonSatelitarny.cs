﻿using Lab3.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    public class TelefonSatelitarny : IGlosnik, IWyswietlacz, IZasilacz
    {

        public void GrajDzwiek()
        {
            Console.WriteLine("jest dzwiek");
        }
        public void UstawGlosnosc(int glosnosc)
        {
            Console.WriteLine("Glosnosc ustawiona na " + glosnosc);
        }
        public void Wyswietl()
        {
            Console.WriteLine("Wyswietlone");
        }
        public void UstawJasnosc(int jasnosc)
        {
            Console.WriteLine("Jasnosc ustawiona na " + jasnosc);
        }
        public int DajPrad()
        {
            Console.WriteLine("Zasilanie dostarczone");
            return 220;
        }
        public void Wylacz()
        {
            Console.WriteLine("telefon wylaczony");
        }
        void IZasilacz.Wylacz()
        {
            this.WylaczInaczej();
        }
    }
}
