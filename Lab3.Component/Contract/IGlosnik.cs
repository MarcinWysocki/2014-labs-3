﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Contract
{
    public interface IGlosnik
    {
        void GrajDzwiek();
        void UstawGlosnosc(int glosnosc);
    }
}
